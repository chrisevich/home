# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

# User specific environment and startup programs

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
export EDITOR=/usr/bin/vim
export VIEWER=/usr/bin/vim
export SYSTEMD_LESS=FRXMK
export SYSTEMD_PAGER=less
export GOPATH="$HOME/go"
export PATH=$HOME/bin:$GOPATH/bin:$HOME/.local/bin:$PATH

if [[ -n $(type -P gpg-agent) ]] && [[ -n "$XDG_RUNTIME_DIR" ]]; then

  export GPGAGENT_ENV_FILE="$XDG_RUNTIME_DIR/gpg-agent.env"
  if [[ ! -S $(gpgconf --list-dirs agent-socket) ]]; then
    echo "Spawning new GPG agent process" > /dev/stderr
    gpg-agent --quiet --batch --no-grab --keep-tty --sh --daemon > "$GPGAGENT_ENV_FILE"
  fi

  export SSHAGENT_ENV_FILE="$XDG_RUNTIME_DIR/ssh-agent.env"
  if [[ ! -S "$XDG_RUNTIME_DIR/ssh-agent.socket" ]]; then
    echo "Spawning new SSH agent process" > /dev/stderr
    ssh-agent -a "$XDG_RUNTIME_DIR/ssh-agent.socket" > "$SSHAGENT_ENV_FILE"
    sed -i -r -e 's/^echo.*$//g' "$SSHAGENT_ENV_FILE"
  fi

  set -a
      source "$GPGAGENT_ENV_FILE"
      source "$SSHAGENT_ENV_FILE"
  set +a

  # Make sure git knows how to talk to the console
  export GPG_TTY=$(tty)
  echo UPDATESTARTUPTTY | gpg-connect-agent
fi
