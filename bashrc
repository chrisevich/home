# .bashrc

# Source global definitions
if [[ -f /etc/bashrc ]]; then
  . /etc/bashrc
fi

# File dropped by environment orchestration (i.e. devel container entrypoint)
if [[ -r "$HOME/.include.env" ]]; then
  . "$HOME/.include.env"
fi

alias diff='diff -Naur'
alias sshx='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no -F /dev/null'
alias scpx='scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no -F /dev/null'
alias ll='ls -l'
alias la='ls -A'
alias dd='dd status=progress'

# This is broken on some distros (CentOS Stream9) and in some contexts
export XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR:-/tmp/run-$UID}
if [[ "$XDG_RUNTIME_DIR" == "/tmp/run-$UID" ]] && [[ ! -d "$XDG_RUNTIME_DIR" ]]; then
  mkdir "$XDG_RUNTIME_DIR"
  chmod 700 "$XDG_RUNTIME_DIR"
fi

# Workaround broken wayland behavior
export GSM_SKIP_SSH_AGENT_WORKAROUND=1

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# The next line updates PATH for the Google Cloud SDK.
if [[ -f "$HOME/google-cloud-sdk/path.bash.inc" ]]; then
  source "$HOME/google-cloud-sdk/path.bash.inc"
fi

# The next line enables shell command completion for gcloud.
if [[ -f "$HOME/google-cloud-sdk/completion.bash.inc" ]]; then
  source "$HOME/google-cloud-sdk/completion.bash.inc"
fi

if [[ -n $(type -P podman) ]]; then
  eval $(podman completion bash)
fi

if [[ -n $(type -P starship) ]]; then
  # Initialize starship prompt (requires installing 'starship')
  # Ref: https://starship.rs/guide/#%F0%9F%9A%80-installation
  eval "$(starship init bash)"
fi
